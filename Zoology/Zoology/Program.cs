﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Xml.Serialization;

namespace Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initialize a list with animals for the zoo
            List<Animal> animals = new List<Animal>() { };
            AnimalList.InitializeAnimals(animals);
            PrintInfoAboutZooAnimals(animals);

            bool runProgram = true;
            //Main loop
            while(runProgram)
            {
                Console.WriteLine("- to exit program type \"e\" to filter the animals list type \"f\"" +
                    " To print the list with animals type \"p\"");
                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "e":
                        runProgram = false;
                        break;
                    case "f":
                        Filtering.FilterAnimalsList(animals);
                        break;
                    case "p":
                        PrintInfoAboutZooAnimals(animals);
                        break;
                    default:
                        break;
                }
            }
        }
 
        public static void PrintInfoAboutZooAnimals(List<Animal> animals)
        {
            foreach (Animal animal in animals)
            {
                Console.WriteLine($"{animal.GetType().Name}, type: {animal.Name}, age: {animal.Age} weight: " +
                    $"{animal.Weight}");
                animal.Talk();
                Console.WriteLine();
            }
        }
    }
}
