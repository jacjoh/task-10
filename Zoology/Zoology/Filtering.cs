﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Zoology
{
    class Filtering
    {
        public static void FilterAnimalsList(List<Animal> animals)
        {
            Console.WriteLine("- To exit filtering type \"e\"");
            Console.WriteLine("- To print the list of animals type \"p\"");
            while (true)
            {
                Console.WriteLine("- Filtering takes three arguments:");
                Console.WriteLine("- What to filter, \"age\" or \"weight\"");
                Console.WriteLine("- \"over\" or \"under\"");
                Console.WriteLine("- And finally a number");
                
                string[] filterCriteria = Console.ReadLine().Split(" ");

                //exit filtering
                if (filterCriteria[0] == "e")
                    break;
                //print the info about the animals
                else if (filterCriteria[0] == "p")
                    Program.PrintInfoAboutZooAnimals(animals);


                //There must be three arguments for the filtering
                if (filterCriteria.Length == 3)
                {
                    if (Int32.TryParse(filterCriteria[2], out int number))
                    {
                        Filter(animals, filterCriteria[0], filterCriteria[1], number);
                    }
                }
            }
        }

        private static void Filter(List<Animal> animals, string filterName, string filterCriteria, int number)
        {
            //Different query based on the arguments passed
            if (filterName == "age" && filterCriteria == "over")
            {
                var linqQuery = from animal in animals
                                where animal.Age > number
                                select animal;
                PrintFilterResult(linqQuery);
            }
            else if(filterName == "age" && filterCriteria == "under")
            {
                var linqQuery = from animal in animals
                                where animal.Age < number
                                select animal;
                PrintFilterResult(linqQuery);
            }
            else if (filterName == "weight" && filterCriteria == "over")
            {
                var linqQuery = from animal in animals
                                where animal.Weight > number
                                select animal;
                PrintFilterResult(linqQuery);
            }
            else if (filterName == "weight" && filterCriteria == "under")
            {
                var linqQuery = from animal in animals
                                where animal.Weight < number
                                select animal;
                PrintFilterResult(linqQuery);
            }
            else
            {
                //Wrong filtering arguments so just returning
                return;
            }
            
        }

        private static void PrintFilterResult(IEnumerable<Animal> linqQuery)
        {
            if(linqQuery.Count() == 0)
            {
                Console.WriteLine("- There were no results to your search");
                return;
            }

            foreach (var animal in linqQuery)
            {
                Console.Write($"{animal.GetType().Name}, name: {animal.Name}, age: {animal.Age}, " +
                    $"weight: {animal.Weight}\r\n");
                Console.WriteLine();
            }
        }

    }
}
