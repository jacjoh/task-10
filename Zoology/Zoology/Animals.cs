﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    abstract class Animal
    {
        public bool CanFly { get; set; }
        public int Weight { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public abstract void Talk();
    }

    class Dog : Animal, IRunner<Dog>
    {
        public Dog()
        {
            Name = "Bulldog";
            Weight = 25;
            Age = 3;
            CanFly = false;
        }
        //Can create a dog with random age and weight
        public Dog(string name)
        {
            Random rnd = new Random();
            Name = name;
            Weight = rnd.Next(5, 15);
            Age = rnd.Next(1, 12);
            CanFly = false;
        }
        public Dog(string name, int weight, int age)
        {
            Name = name;
            Weight = weight;
            Age = age;
            CanFly = false;
        }
        public void Run()
        {
            Console.WriteLine($"{Name} is running very fast");
        }

        public override void Talk()
        {
            Console.WriteLine($"{Name}: \"Bark Bark\""); ;
        }
    }

    class Bird: Animal, IFlyer<Bird>
    {
        public Bird()
        {
            Name = "Dove";
            Weight = 1;
            CanFly = true;
            Age = 4;
        }
        // Can add a bird with random age and weight
        public Bird(string name)
        {
            Random rnd = new Random();
            Name = name;
            Weight = rnd.Next(1, 10);
            Age = rnd.Next(1, 12);
            CanFly = false;
        }
        public Bird(string name, int weight, int age, bool fly)
        {
            Name = name;
            Weight = weight;
            CanFly = fly;
            Age = age;
        }

        public override void Talk()
        {
            Console.WriteLine($"{Name}: \"ChirpChirp\""); ;
        }

        public void Fly()
        {
            if (CanFly)
            {
                Console.WriteLine($"{Name} flies away");
            }
            else
            {
                Console.WriteLine($"{Name} flutter with wings, but does not work! Must run");
            }
        }
    }
}
