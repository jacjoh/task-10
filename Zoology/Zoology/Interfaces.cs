﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public interface IFlyer<T>
    {
        public void Fly();
    }
    
    public interface IRunner<T>
    {
        public void Run();
    }
}
