﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    class AnimalList
    {
        public static void InitializeAnimals(List<Animal> animals)
        {
            Console.WriteLine("You can either Select an existing set of animals, or enter your own set of animals");
            while (true)
            {
                Console.WriteLine("To select an existing set type \"generate\" and to create an own list type \"add\"");
                string choice = Console.ReadLine();

                //Can either add animals themself, or get a predefined list
                if (choice == "generate")
                {
                    GenerateAnimals(animals);
                    break;
                }
                else if (choice == "add")
                {
                    AddOwnAnimals(animals);
                    break;
                }
            }
            
        }
        private static void AddOwnAnimals(List<Animal> animals)
        {
            //Loops to let a person add more than one animal
            Console.WriteLine("- To add an animal type either Dog or Bird, and the name");
            Console.WriteLine("- To finish adding animals type e");

            while (true)
            {
                string animal = Console.ReadLine();
                if(animal.Length != 0)
                {
                    string animalType = animal.Split(" ")[0];
                    int indexOfName =  animal.IndexOf(" ") + 1;
                    //Make it a substring if the animal has more than one name
                    string animalName = animal.Substring(indexOfName);

                    Console.WriteLine(animalName);
                    if (animalType == "e")
                    {
                        //There should be at least 6 animals in the list before one can exit
                        if (!(animals.Count >= 6))
                        {
                            Console.WriteLine($"- You must input 6 animals, current list holds {animals.Count} animals");
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        //if the index is negative there were no animal name input from the user
                        if(indexOfName > 0)
                            AddAnimalToList(animals, animalType, animalName);
                    }
                }
            }
        }

        private static void AddAnimalToList(List<Animal> animals, string animalType, string animalName)
        {
            //Must avoid adding an empty string as name
            if (animalName.Length == 0)
            {
                Console.WriteLine("- Must input either dog or bird, and the name of the dog or bird");
                return;
            }

            if(animalType == "Dog")
            {
                animals.Add(new Dog(animalName));
                Console.WriteLine($"{animalName} added to the list");
            }
            else if (animalType == "Bird")
            {
                animals.Add(new Bird(animalName));
                Console.WriteLine($"{animalName} added to the list");
            }
            else
            {
                Console.WriteLine("- The first argument must be either Dog or Bird");
            }
        }

        private static void GenerateAnimals(List<Animal> animals)
        {
            animals.Add(new Dog("Pug", 9, 4));
            animals.Add(new Dog());
            animals.Add(new Bird("Ostrich", 120, 5, false));
            animals.Add(new Bird("Parrot", 4, 2, true));
            animals.Add(new Dog("Poodle", 20, 10));
            animals.Add(new Bird());
        }
    }
}
